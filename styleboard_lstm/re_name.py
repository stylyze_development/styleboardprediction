OLD_CHECKPOINT_FILE = "model/model_final/model.ckpt-34865"
NEW_CHECKPOINT_FILE = "model/model_final/model2.ckpt-34865"

import tensorflow as tf

vars_to_rename = {
    "lstm/BW/BasicLSTMCell/Linear/Matrix": "lstm/BW/basic_lstm_cell/kernel",
    "lstm/BW/BasicLSTMCell/Linear/Bias": "lstm/BW/basic_lstm_cell/bias",
    "lstm/FW/BasicLSTMCell/Linear/Matrix": "lstm/FW/basic_lstm_cell/kernel",
    "lstm/FW/BasicLSTMCell/Linear/Bias": "lstm/FW/basic_lstm_cell/bias",
}

# vars_to_rename = {
#     "lstm/bw/basic_lstm_cell/kernel": "lstm/BW/basic_lstm_cell/kernel",
#     "lstm/bw/basic_lstm_cell/bias": "lstm/BW/basic_lstm_cell/bias",
#     "lstm/fw/basic_lstm_cell/kernel": "lstm/FW/basic_lstm_cell/kernel",
#     "lstm/fw/basic_lstm_cell/bias": "lstm/FW/basic_lstm_cell/bias",
# }

new_checkpoint_vars = {}

reader = tf.train.NewCheckpointReader(OLD_CHECKPOINT_FILE)

for old_name in reader.get_variable_to_shape_map():

    if old_name in vars_to_rename:
        new_name = vars_to_rename[old_name]
    else:
        new_name = old_name

    new_checkpoint_vars[new_name] = tf.Variable(reader.get_tensor(old_name))

init = tf.global_variables_initializer()
saver = tf.train.Saver(new_checkpoint_vars)

with tf.Session() as sess:

    sess.run(init)
    saver.save(sess, NEW_CHECKPOINT_FILE)


# import the inspect_checkpoint library
from tensorflow.python.tools import inspect_checkpoint as chkp

# print all tensors in checkpoint file
chkp.print_tensors_in_checkpoint_file("model/model_final/model2.ckpt-34865", tensor_name='', all_tensors=True)

