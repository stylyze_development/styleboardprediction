import tensorflow as tf
import numpy as np
import json
from util.load_data_multimodal_inference import load_num_category, load_graph, load_train_data, load_train_size, load_fitb_data, load_test_size, load_auc_data, load_inference_size
from model.model_multimodal import GNN
from  datetime import *
import time
import pickle
from tensorflow.contrib.rnn import GRUCell
import os
##################load data###################
# ftrain = open('/home/paperspace/NGNN-master/data/train_no_dup_new_100.json', 'r')
# train_outfit_list = json.load(ftrain)
ftest = open('/home/paperspace/NGNN-master/data/test_no_dup_new_100.json', 'r')
test_outfit_list = json.load(ftest)

finference = open('/home/paperspace/NGNN-master/data/inference_no_dup_new_100.json', 'r')
inference_outfit_list = json.load(finference)

def cm_ggnn(batch_size, image_hidden_size, text_hidden_size, n_steps, learning_rate, G, num_category, opt, i, beta):

    os.environ["CUDA_VISIBLE_DEVICES"] = '0'
    with tf.Session() as sess:

        # initialize the graph
        # 2017-03-02 if using tensorflow >= 0.12
        if int((tf.__version__).split('.')[1]) < 12 and int((tf.__version__).split('.')[0]) < 1:
            init = tf.initialize_all_variables()
        else:
            init = tf.global_variables_initializer()
        sess.run(init)

        new_saver = tf.train.import_meta_graph("/home/paperspace/NGNN-master/multi_modal_2/cm_ggnn.ckpt.meta")
        new_saver.restore(sess, tf.train.latest_checkpoint("/home/paperspace/NGNN-master/multi_modal_2/"))
        graph = tf.get_default_graph()

        # tensors = [n.name for n in tf.get_default_graph().as_graph_def().node]
        # print(tensors)

        image_pos = graph.get_tensor_by_name("image_pos:0")
        text_pos = graph.get_tensor_by_name("text_pos:0")
        graph_pos = graph.get_tensor_by_name("graph_pos:0")
        s_pos = graph.get_tensor_by_name("s_pos:0")

        ##### AUC #####
        test_size_auc = load_test_size()
        batches = int((test_size_auc * 2) / batch_size)
        right = 0.
        score_test = []
        score_test_2 = []
        outfit_out_all = []
        score_all = []
        for i in range(batches):
            test_auc = load_auc_data(i, batch_size, test_outfit_list)
            answer = [s_pos.eval(feed_dict={image_pos: test_auc[0], text_pos: test_auc[1], graph_pos: test_auc[2]})]
            answer = np.asarray(answer[0])
            outfit_out_all += test_auc[3]

            for j in range(int(batch_size / 2)):
                a = []
                for k in range(j * 2, (j + 1) * 2):
                    a.append(answer[k][0])
                score_test.append(a[0])
                score_test_2.append(a[1])
                score_all += [a[0]]
                score_all += [a[1]]
                 
                if np.argmax(a) == 0:
                    right += 1.

        print(answer)
        print("testset size: " + str(test_size_auc))
        auc = float(right / test_size_auc)
        avg_score = sum(score_test)/len(score_test)
        avg_score_2 = sum(score_test_2)/len(score_test_2)
        min_score = min(score_test) 
        min_score_2 = min(score_test_2)
        max_score = max(score_test) 
        max_score_2 = max(score_test_2) 
        print("test auc: %f" % auc)
        print("test avg score: %f" % avg_score)
        print("test avg score neg: %f" % avg_score_2)
        print("test min score: %f" % min_score)
        print("test min score neg: %f" % min_score_2)
        print("test max score: %f" % max_score)
        print("test max score neg: %f" % max_score_2)
        print(len(score_test))
        print(len(score_test_2))
        print(len(outfit_out_all))
        print(len(score_all))

        with open("/home/paperspace/NGNN-master/outfit_out_all", "wb") as my_file:
            pickle.dump(outfit_out_all, my_file, protocol=2)

        with open("/home/paperspace/NGNN-master/score_all", "wb") as my_file:
            pickle.dump(score_all, my_file, protocol=2)  

        ##### AUC #####
        inference_size_auc = load_inference_size()
        batches = int((inference_size_auc * 2) / batch_size)
        right = 0.
        score_inference = []
        score_inference_2 = []
        outfit_out_all_infe = []
        score_all_infe = []
        for i in range(batches):
            inference_auc = load_auc_data(i, batch_size, inference_outfit_list)
            answer = [s_pos.eval(feed_dict={image_pos: inference_auc[0], text_pos: inference_auc[1], graph_pos: inference_auc[2]})]
            answer = np.asarray(answer[0])
            outfit_out_all_infe += inference_auc[3]

            for j in range(int(batch_size / 2)):
                a = []
                for k in range(j * 2, (j + 1) * 2):
                    a.append(answer[k][0])
                score_inference.append(a[0])
                score_inference_2.append(a[1])
                score_all_infe += [a[0]]
                score_all_infe += [a[1]]

                if np.argmax(a) == 0:
                    right += 1.

        print(answer)
        print("inference set size: " + str(inference_size_auc))
        inference_auc = float(right / inference_size_auc)
        inference_avg_score = sum(score_inference)/len(score_inference)
        inference_avg_score_2 = sum(score_inference_2)/len(score_inference_2)
        min_score_inference = min(score_inference) 
        min_score_inference_2 = min(score_inference_2)
        max_score_inference = max(score_inference) 
        max_score_inference_2 = max(score_inference_2) 
        print("inference auc: %f" % inference_auc)
        print("inference avg score: %f" % inference_avg_score)
        print("inference avg score neg: %f" % inference_avg_score_2)
        print("inference min score: %f" % min_score_inference)
        print("inference min score neg: %f" % min_score_inference_2)
        print("inference max score: %f" % max_score_inference)
        print("inference max score neg: %f" % max_score_inference_2)
        print(len(score_inference))
        print(len(score_inference_2))
        print(len(outfit_out_all_infe))
        print(len(score_all_infe))

        with open("/home/paperspace/NGNN-master/outfit_out_all_infe", "wb") as my_file:
            pickle.dump(outfit_out_all_infe, my_file, protocol=2)

        with open("/home/paperspace/NGNN-master/score_all_infe", "wb") as my_file:
            pickle.dump(score_all_infe, my_file, protocol=2)


if __name__ == '__main__':

    num_category = load_num_category()
    G = load_graph()
    best_accurancy = 0.
    i = 0
    batch_size = 16
    image_hidden_size = 12
    text_hidden_size = 12
    n_steps = 3
    learning_rate = 0.001
    opt = "RMSProp"
    beta = 0.2
    cm_ggnn(batch_size, image_hidden_size, text_hidden_size, n_steps, learning_rate, G, num_category, opt, i, beta)

