import numpy as np
import pandas as pd
import pickle
import os
import re
import sys
import json
import csv
import shutil
import os.path
from os import path

cwd = os.getcwd()

with open("/opt/tomcat/blobs/export/nmqa/files/csbMachineLearning_neimanmarcus_development-neimanmarcus_20210315.json") as json_file:
    data = json.load(json_file)

items = []
counter = 0
counter_bad = 0
content = []
csbs = []
for b in range(len(data)):
	num_items = len(data[b])

	for i in range(num_items):
	    item_tmp = data[b][i]
	    csb_id = item_tmp["id"]
	    lsbs_tmp = item_tmp['children']
	    lsb = [""]*5
	    bad_image = "false"

	    if len(lsbs_tmp) > 6:
	    	bad_image = "true"

	    for j in range(len(lsbs_tmp)):
	        lsb_tmp = lsbs_tmp[j]
	        if lsb_tmp["categories"] in [['Sweaters'],['Tops']]: 
	            lsb[0] = lsb_tmp["imageId"]
	            if os.path.isfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[0] + ".jpg"):
	            	shutil.copyfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[0] + ".jpg", "/opt/tomcat/blobs/export/nmqa/images_modified/" + lsb[0] + ".jpg")
	            else:
	            	bad_image = "true"
	        # if lsb_tmp["categories"] in [['Pants'],['Jeans']]: 
	        #     lsb[1] = lsb_tmp["imageId"]
	        #     if os.path.isfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[1] + ".jpg"):
	        #     	shutil.copyfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[1] + ".jpg", "/opt/tomcat/blobs/export/nmqa/images_modified_b/" + lsb[1] + ".jpg")
	        #     else:
	        #     	bad_image = "true"
	        # elif lsb_tmp["categories"] in [['Booties'],['Pumps'],['Shoes'],['Sandals']]: 
	        #     lsb[2] = lsb_tmp["imageId"]
	        #     if os.path.isfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[2] + ".jpg"):
	        #     	shutil.copyfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[2] + ".jpg", "/opt/tomcat/blobs/export/nmqa/images_modified/" + lsb[2] + ".jpg")
	        #     else:
	        #     	bad_image = "true"
	        # elif lsb_tmp["categories"] in [['Satchels'],['Shoulder Bags'],['Crossbody Bags'],['Top Handle Bags'],['Bags & Leather Goods']]: 
	        #     lsb[3] = lsb_tmp["imageId"] 
	        #     if os.path.isfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[3] + ".jpg"):
	        #     	shutil.copyfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[3] + ".jpg", "/home/tfuser/polyvore-master/data/images/" + csb_id + "/3.jpg")
	        #     else:
	        #     	bad_image = "true"
	        # elif lsb_tmp["categories"] in [['Jewelry'],['Rings'],['Earrings'],['Accessories'],['Necklaces'],['Bracelets']]:  
	        #     lsb[4] = lsb_tmp["imageId"] 
	        #     if os.path.isfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[4] + ".jpg"):
	        #     	shutil.copyfile("/opt/tomcat/blobs/export/nmqa/images/" + lsb[4] + ".jpg", "/home/tfuser/polyvore-master/data/images/" + csb_id + "/4.jpg")
	        #     else:
	        #     	bad_image = "true"    	        