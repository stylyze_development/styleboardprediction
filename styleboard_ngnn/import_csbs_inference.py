import numpy as np
import pandas as pd
import pickle
import os
import re
import sys
import json
import csv
import shutil
import os.path
from os import listdir
from os.path import join
import random
from PIL import Image

cwd = os.getcwd()

df = pd.read_csv("/home/tfuser/visuallysimilar/visually_similar_v1/data_tables/Visually_Similar_neimanmarcus_qaApproved_qaUnapproved_extended_05132021.csv")

with open("/opt/tomcat/blobs/export/nm/files/csbMachineLearning_neimanmarcus_staging-neimanmarcus_20210513.json") as json_file:
	data = json.load(json_file)

with open("/opt/tomcat/blobs/export/nm/str_to_id", "rb") as my_file2:
    str_to_id = pickle.load(my_file2)

random.shuffle(data)
items = []
counter = 0
counter_bad = 0
bad_image_counter = 0
for b in range(len(data)):
	num_items = len(data[b])

	for i in range(num_items):
		item_tmp = data[b][i]
		csb_id = item_tmp["id"]
		likes = int(item_tmp["likes"])
		lsbs_tmp = item_tmp['children']
		lsb = [""]*len(lsbs_tmp)
		title = [""]*len(lsbs_tmp)
		categories = [""]*len(lsbs_tmp)
		buffer_ = []
		bad_image = "false"

		if likes == 0 and len(items) < 424*4 and len(lsbs_tmp) <= 8 and len(lsbs_tmp) >= 3:
		# if likes == 0:
			for j in range(len(lsbs_tmp)):
				lsb_tmp = lsbs_tmp[j]
				if lsb_tmp["categories"][0] in list(str_to_id.keys()) and lsb_tmp["categories"][0] not in buffer_:
				# if lsb_tmp["categories"][0] in list(str_to_id.keys()):
					lsb[j] = lsb_tmp["imageId"]
					title[j] = lsb_tmp["title"]
					categories[j] = lsb_tmp["categories"][0]
					buffer_.append(lsb_tmp["categories"][0])   	

		flag = "false"
		for j in lsb:
			if j == "":
				flag = "true"

		if os.path.isdir("/opt/tomcat/blobs/export/nm/images_inference/" + csb_id):
			flag = "true"
			print("duplicated csb: " + str(csb_id))
		
		if flag == "true":
			counter_bad += 1
				
		if flag != "true":
			os.mkdir("/opt/tomcat/blobs/export/nm/images_inference/" + csb_id)
			for lsb_i in range(len(lsbs_tmp)):
				object_id =  lsb[lsb_i]
				if len(df[df["OBJECT_ID"] == object_id]["CURATOR_IMAGE_PATH"].values) == 1:
					image_file = df[df["OBJECT_ID"] == object_id]["CURATOR_IMAGE_PATH"].values[0]
					if os.path.isfile(image_file):
						try:
							img = Image.open(image_file)
							img = img.resize((229, 229))
							shutil.copyfile(image_file, "/opt/tomcat/blobs/export/nm/images_inference/" + csb_id + "/" + str(lsb_i) + ".jpg")
						except:
							print("bad file found!")
							print(image_file)
							bad_image = "true"
							bad_image_counter += 1
					else:
						print("no file found!")
						print(image_file)
						bad_image = "true"
						bad_image_counter += 1

				else:
					bad_image = "true"
					bad_image_counter += 1

			# if len(listdir("/opt/tomcat/blobs/export/nm/images/" + csb_id)) != len(lsbs_tmp):
			# 	print("***")
			# 	print(csb_id)
			# 	print(lsb)

			if bad_image != "true":        
				item = {}
				item["set_id"] = csb_id
				item["items"] = []
				for lsb_j in range(len(lsbs_tmp)):
					item["items"].append({"index": lsb_j, "name": title[lsb_j], "categoryid": categories[lsb_j]})
				  
				items.append(item)
			 
		counter += 1

print(len(items))
print(bad_image_counter)
print(counter)
print(counter_bad)
			
with open("/opt/tomcat/blobs/export/nm/" + "inference_no_dup.json", 'w') as f:
	json.dump(items, f)   

		  