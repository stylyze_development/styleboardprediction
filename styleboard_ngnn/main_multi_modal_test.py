import tensorflow as tf
import numpy as np
import json
from util.load_data_multimodal import load_num_category, load_graph, load_train_data, load_train_size, load_fitb_data, load_test_size, load_auc_data
from model.model_multimodal import GNN
from  datetime import *
import time
import pickle
from tensorflow.contrib.rnn import GRUCell
import os
##################load data###################
# ftrain = open('/home/paperspace/NGNN-master/data/train_no_dup_new_100.json', 'r')
# train_outfit_list = json.load(ftrain)
ftest = open('/home/paperspace/NGNN-master/data/test_no_dup_new_100.json', 'r')
test_outfit_list = json.load(ftest)

def cm_ggnn(batch_size, image_hidden_size, text_hidden_size, n_steps, learning_rate, G, num_category, opt, i, beta):

    os.environ["CUDA_VISIBLE_DEVICES"] = '0'
    with tf.Session() as sess:

        # initialize the graph
        # 2017-03-02 if using tensorflow >= 0.12
        if int((tf.__version__).split('.')[1]) < 12 and int((tf.__version__).split('.')[0]) < 1:
            init = tf.initialize_all_variables()
        else:
            init = tf.global_variables_initializer()
        sess.run(init)

        new_saver = tf.train.import_meta_graph("/home/paperspace/NGNN-master/multi_modal_2/cm_ggnn.ckpt.meta")
        new_saver.restore(sess, tf.train.latest_checkpoint("/home/paperspace/NGNN-master/multi_modal_2/"))
        graph = tf.get_default_graph()

        # tensors = [n.name for n in tf.get_default_graph().as_graph_def().node]
        # print(tensors)

        image_pos = graph.get_tensor_by_name("image_pos:0")
        text_pos = graph.get_tensor_by_name("text_pos:0")
        graph_pos = graph.get_tensor_by_name("graph_pos:0")
        s_pos = graph.get_tensor_by_name("s_pos:0")

        ##### AUC #####
        test_size_auc = load_test_size()
        batches = int((test_size_auc * 2) / batch_size)
        right = 0.
        for i in range(batches):
            test_auc = load_auc_data(i, batch_size, test_outfit_list)
            answer = [s_pos.eval(feed_dict={image_pos: test_auc[0], text_pos: test_auc[1], graph_pos: test_auc[2]})]
            answer = np.asarray(answer[0])

            for j in range(int(batch_size / 2)):
                a = []
                for k in range(j * 2, (j + 1) * 2):
                    a.append(answer[k][0])
                if np.argmax(a) == 0:
                    right += 1.

        print(answer)
        print("testset size: " + str(test_size_auc))
        auc = float(right / test_size_auc)
        print("auc: %f" % auc)


if __name__ == '__main__':

    num_category = load_num_category()
    G = load_graph()
    best_accurancy = 0.
    i = 0
    batch_size = 16
    image_hidden_size = 12
    text_hidden_size = 12
    n_steps = 3
    learning_rate = 0.001
    opt = "RMSProp"
    beta = 0.2
    cm_ggnn(batch_size, image_hidden_size, text_hidden_size, n_steps, learning_rate, G, num_category, opt, i, beta)

