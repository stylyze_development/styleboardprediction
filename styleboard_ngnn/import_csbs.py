import numpy as np
import pandas as pd
import pickle
import os
import re
import sys
import json
import csv
import shutil
import os.path
from os import listdir
from os.path import join

cwd = os.getcwd()

df = pd.read_csv("/home/tfuser/visuallysimilar/visually_similar_v1/data_tables/Visually_Similar_neimanmarcus_qaApproved_qaUnapproved_extended_04152021.csv")

with open("/opt/tomcat/blobs/export/nm/files/csbMachineLearning_neimanmarcus_staging-neimanmarcus_20210415.json") as json_file:
	data = json.load(json_file)

items = []
counter = 0
counter_bad = 0
bad_image_counter = 0
for b in range(len(data)):
	num_items = len(data[b])

	for i in range(num_items):
		item_tmp = data[b][i]
		csb_id = item_tmp["id"]
		likes = int(item_tmp["likes"])
		lsbs_tmp = item_tmp['children']
		lsb = [""]*5
		title = [""]*5
		categories = [""]*5
		bad_image = "false"

		if len(lsbs_tmp) == 5 and likes > 0:
			for j in range(len(lsbs_tmp)):
				lsb_tmp = lsbs_tmp[j]
				if lsb_tmp["categories"] in [['Sweaters'],['Tops']]: 
					lsb[0] = lsb_tmp["imageId"]
					title[0] = lsb_tmp["title"]
					categories[0] = lsb_tmp["categories"][0]
					
				elif lsb_tmp["categories"] in [['Pants'],['Jeans']]: 
					lsb[1] = lsb_tmp["imageId"]
					title[1] = lsb_tmp["title"]
					categories[1] = lsb_tmp["categories"][0]
					 
				elif lsb_tmp["categories"] in [['Booties'],['Pumps'],['Shoes'],['Sandals']]: 
					lsb[2] = lsb_tmp["imageId"]
					title[2] = lsb_tmp["title"]
					categories[2] = lsb_tmp["categories"][0]
					 
				elif lsb_tmp["categories"] in [['Satchels'],['Shoulder Bags'],['Crossbody Bags'],['Top Handle Bags'],['Bags & Leather Goods']]: 
					lsb[3] = lsb_tmp["imageId"]
					title[3] = lsb_tmp["title"]
					categories[3] = lsb_tmp["categories"][0] 
					 
				elif lsb_tmp["categories"] in [['Jewelry'],['Rings'],['Earrings'],['Accessories'],['Necklaces'],['Bracelets']]:  
					lsb[4] = lsb_tmp["imageId"]
					title[4] = lsb_tmp["title"]
					categories[4] = lsb_tmp["categories"][0]    	

		flag = "false"
		for j in lsb:
			if j == "":
				flag = "true"
		
		if flag == "true":
			counter_bad += 1
				
		if flag != "true":
			os.mkdir("/opt/tomcat/blobs/export/nm/images/" + csb_id)
			for lsb_i in range(5):
				# mypath = "/opt/tomcat/blobs/neimanmarcus/" + lsb[lsb_i]
				# image_file = [f for f in listdir(mypath) if join(mypath, f).endswith("jpg")]
				# if os.path.isfile(mypath + "/" + image_file[0]):
				# 	shutil.copyfile(mypath + "/" + image_file[0], "/opt/tomcat/blobs/export/nm/images/" + csb_id + "/" + str(lsb_i) + ".jpg")

				object_id =  lsb[lsb_i]
				if len(df[df["OBJECT_ID"] == object_id]["CURATOR_IMAGE_PATH"].values) == 1:
					image_file = df[df["OBJECT_ID"] == object_id]["CURATOR_IMAGE_PATH"].values[0]
					if os.path.isfile(image_file):
						shutil.copyfile(image_file, "/opt/tomcat/blobs/export/nm/images/" + csb_id + "/" + str(lsb_i) + ".jpg")
					else:
						print("no file found!")
						print(image_file)

				else:
					bad_image = "true"
					bad_image_counter += 1

			if len(listdir("/opt/tomcat/blobs/export/nm/images/" + csb_id)) != 5:
				print("***")
				print(csb_id)
				print(lsb)

			if bad_image != "true":        
				item = {}
				item["items"] = [
					{
						"index": 0, 
						"name": title[0], 
						"categoryid": categories[0]
					}, 
					{
						"index": 1, 
						"name": title[1], 
						"categoryid": categories[1]
					}, 
					{
						"index": 2, 
						"name": title[2],  
						"categoryid": categories[2]
					}, 
					{
						"index": 3, 
						"name": title[3], 
						"categoryid": categories[3]
					},
					{
						"index": 4, 
						"name": title[4],  
						"categoryid": categories[4]
					}
				]
				item["set_id"] = csb_id
				
				items.append(item)
			 
		
		counter += 1

print(len(items))
print(bad_image_counter)
print(counter)
print(counter_bad)
			
with open("/opt/tomcat/blobs/export/nm/" + "test_no_dup.json", 'w') as f:
	json.dump(items, f)   

		  